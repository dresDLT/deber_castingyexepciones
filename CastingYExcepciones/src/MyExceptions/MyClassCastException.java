package MyExceptions;

public class MyClassCastException extends ClassCastException{
	
//	public MyClassCastException (){
//		this("Cast Incorrecto - ");
//	}

	public MyClassCastException(String str) {
		super("Casting Exception - " + str);
	}
	
}
