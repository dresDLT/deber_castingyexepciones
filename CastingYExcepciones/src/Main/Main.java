package Main;

import Clases.Apple;
import Clases.Citrus;
import Clases.Fruit;
import Clases.Orange;
import Interfaces.Squeezable;
import MyExceptions.MyClassCastException;

public class Main {

	public static void main(String[] args) {
	
		
		//Your application should construct one instance of each of the following classes:
		Object objeto = new Object();
		
		Fruit fruta = new Fruit();
		
		Apple manzana = new Apple();
		
		Citrus citrico = new Citrus();
		
		Orange naranja = new Orange();
		
		Squeezable squeezable = new Squeezable() { };
		
		
		//Tratar de hacer cast a cada uno de los objetos con los siguientes tipos ()
		//Try to cast each of these objects to the following types:
		
		//Object
		objeto = fruta;
		objeto = manzana;
		objeto = citrico;
		objeto = naranja;
		objeto = squeezable;
		
		//Fruit-----------------------------------------------------------------
		
		try {
			//fruta = (Fruit) objeto;
			cast(fruta, objeto);
			System.out.println("1. Hecho");
		} catch (MyClassCastException myE) {
			System.out.println("1." + myE.getMessage());

		}
		
		fruta = manzana;
		fruta = citrico;
		fruta = naranja;
		
		try {
			//fruta = (Fruit) squeezable;
			cast(fruta, squeezable);
			System.out.println("2. Hecho");
		} catch (MyClassCastException myE) {
			System.out.println("2." + myE.getMessage());

		}

		
		//Apple-----------------------------------------------------------------		
		try {
			//manzana = (Apple) objeto;
			cast(manzana, objeto);
			System.out.println("3. Hecho");
		} catch (MyClassCastException myE) {
			System.out.println("3." + myE.getMessage());

		}
		
		try {
			//manzana = (Apple) fruta;
			cast(manzana, fruta);
			System.out.println("4. Hecho");
		} catch (MyClassCastException myE) {
			System.out.println("4." + myE.getMessage());

		}
		
		try {
			//			manzana = naranja;				//<----oJO
			cast(manzana, naranja);
			System.out.println("5. Hecho");
		} catch (MyClassCastException myE) {
			System.out.println("5." + myE.getMessage()+"--------------------------no las deja java");
		}
		
		
		try {
			//			manzana = citrico;				//<----oJO
			cast(manzana, citrico);
			System.out.println("6. Hecho");
		} catch (MyClassCastException myE) {
			System.out.println("6." + myE.getMessage()+"--------------------------no las deja java");
		}
		
		try {
			//manzana = (Apple) squeezable;
			cast(manzana, squeezable);
			System.out.println("7. Hecho");
		} catch (MyClassCastException myE) {
			System.out.println("7." + myE.getMessage());

		}
		
		//Citrus-----------------------------------------------------------------

		
		try {
			//citrico = (Citrus) objeto;
			cast(citrico, objeto);
			System.out.println("8. Hecho");
		} catch (MyClassCastException myE) {
			System.out.println("8." + myE.getMessage());

		}
		
		try {
			//citrico= (Citrus) fruta;
			cast(citrico, fruta);
			System.out.println("9. Hecho");
		} catch (MyClassCastException myE) {
			System.out.println("9." + myE.getMessage());

		}
		
		try {
			//			citrico = manzana;				//<----oJO
			cast(citrico, manzana);
			System.out.println("10. Hecho");
		} catch (MyClassCastException myE) {
			System.out.println("10." + myE.getMessage()+"--------------------------no las deja java");
		}
		
		citrico = naranja;
		
		try {
			//citrico = (Citrus) squeezable;
			cast(citrico, squeezable);
			System.out.println("11. Hecho");
		} catch (MyClassCastException myE) {
			System.out.println("11." + myE.getMessage());

		}
		
		//Orange-----------------------------------------------------------------
		
		try {
			//naranja = (Orange) objeto;
			cast(naranja, objeto);
			System.out.println("12. Hecho");
		} catch (MyClassCastException myE) {
			System.out.println("12." + myE.getMessage());

		}
		
		try {
			//naranja = (Orange) fruta;
			cast(naranja, fruta);
			System.out.println("13. Hecho");
		} catch (MyClassCastException myE) {
			System.out.println("13." + myE.getMessage());

		}

		try {
			//			naranja = manzana;				//<----oJO
			cast(naranja, manzana);
			System.out.println("14. Hecho");
		} catch (MyClassCastException myE) {
			System.out.println("14." + myE.getMessage()+"--------------------------no las deja java");
		}

		try {
			//naranja = (Orange) citrico;
			cast(naranja, citrico);
			System.out.println("15. Hecho");
		} catch (MyClassCastException myE) {
			System.out.println("15." + myE.getMessage());

		}
		
		try {
			//naranja = (Orange) squeezable;
			cast(naranja, squeezable);
			System.out.println("16. Hecho");
		} catch (MyClassCastException myE) {
			System.out.println("16." + myE.getMessage());

		}
		
		//Squeezable-----------------------------------------------------------------

		
		try {
			//squeezable = (Squeezable) objeto;
			cast(squeezable, objeto);
			System.out.println("17. Hecho");
		} catch (MyClassCastException myE) {
			System.out.println("17." + myE.getMessage());

		}
		
		try {
			//squeezable = (Squeezable) fruta;
			cast(squeezable, fruta);
			System.out.println("18. Hecho");
		} catch (MyClassCastException myE) {
			System.out.println("18." + myE.getMessage());

		}
		
		try {
			//		squeezable = (Squeezable) manzana;
			cast(squeezable, manzana);
			System.out.println("19. Hecho");
		} catch (MyClassCastException myE) {
			System.out.println("19." + myE.getMessage());

		}

		squeezable = citrico;
		squeezable = naranja;

	}
	
	public static void cast (Fruit f, Object o) throws MyClassCastException{
		if (!(o instanceof Fruit)) {
			throw new MyClassCastException(" Fruit:Objet");
		}
	}
	
	public static void cast (Fruit f, Squeezable s) throws MyClassCastException{
		if (!(s instanceof Fruit)) {
			throw new MyClassCastException(" Fruit:Squeezable");
		}
	}
	
	public static void cast (Apple a, Object o) throws MyClassCastException{
		if (!(o instanceof Apple)) {
			throw new MyClassCastException(" Apple:Object");
		}
	}
	
	public static void cast (Apple a, Fruit f) throws MyClassCastException{
		if (!(f instanceof Apple)) {
			throw new MyClassCastException(" Apple:Fruit");
		}
	}
	 
	public static void cast (Apple a, Squeezable s) throws MyClassCastException{
		if (!(s instanceof Apple)) {
			throw new MyClassCastException(" Apple:Squeezable");
		}
	}
	
	public static void cast (Apple a, Citrus c) throws MyClassCastException{
		if (!(a.getClass().isInstance(c))) {
			throw new MyClassCastException(" Apple:Citrus");
		}
	}
	
	public static void cast (Apple a, Orange o) throws MyClassCastException{
		if (!(a.getClass().isInstance(o))) {
			throw new MyClassCastException(" Apple:Orange");
		}
	}
	
	//-----------------------------------------------------------
	
	public static void cast (Citrus c, Object o) throws MyClassCastException{
		if (!(o instanceof Citrus)) {
			throw new MyClassCastException(" Citrus:Object");
		}
	}
	////////////////////////////////////////////////
	public static void cast (Citrus c, Fruit f) throws MyClassCastException{
		if (!(c.getClass().isInstance(f))) {
			throw new MyClassCastException(" Citrus:Fruit");
		}
	}
	
	public static void cast (Citrus c, Apple a) throws MyClassCastException{
		if (!(c.getClass().isInstance(a))) {
			throw new MyClassCastException(" Citrus:Apple");
		}
	}
	
	public static void cast (Citrus c, Squeezable s) throws MyClassCastException{
		if (!(s instanceof Citrus)) {
			throw new MyClassCastException(" Citrus:Squeezable");
		}
	}
	
	public static void cast (Orange or, Object o) throws MyClassCastException{
		if (!(o instanceof Orange)) {
			throw new MyClassCastException(" Orange:Object");
		}
	}
	
	public static void cast (Orange or, Fruit f) throws MyClassCastException{
		if (!(or.getClass().isInstance(f))) {
			throw new MyClassCastException(" Orange:Fruit");
		}
	}
	
	public static void cast (Orange or, Citrus c) throws MyClassCastException{
		if (!(c instanceof Orange)) {
			throw new MyClassCastException(" Orange:Citrus");
		}
	}
	
	public static void cast (Orange o, Apple a) throws MyClassCastException{
		if (!(o.getClass().isInstance(a))) {
			throw new MyClassCastException(" Orange:Apple");
		}
	}
	
	public static void cast (Orange or, Squeezable s) throws MyClassCastException{
		if (!(s instanceof Orange)) {
			throw new MyClassCastException(" Orange:Squeezable");
		}
	}
	
	public static void cast (Squeezable s, Object o) throws MyClassCastException{
		if (!(o instanceof Squeezable)) {
			throw new MyClassCastException(" Squeezable:Object");
		}
	}
	
	public static void cast (Squeezable s, Fruit f) throws MyClassCastException{
		if (!(f instanceof Squeezable)) {
			throw new MyClassCastException(" Squeezable:Fruit");
		}
	}
	
	public static void cast (Squeezable s, Apple a) throws MyClassCastException{
		if (!(a instanceof Squeezable)) {
			throw new MyClassCastException(" Squeezable:Apple");
		}
	}

}
